# Rules
1. Nothing that will crash a Spigot / BungeeCord server
2. Nothing malicious
3. No removing other's work in PRs
4. Nothing that violates GitLab's Terms of Service
5. You may not publish other's work without their explicit permission

# Contributing
1. Clone (`git clone https://gitlab.com/RoyalMC_Devs/RoyalMC_Economy.git`)
3. Make a new package inside org.royalmc.economy with the feature inside of it or edit already present classes as required. It is preferred if you make this new feature within its own branch but this is not a requirement that is set in stone.
4. Make your code
5. Push (`git push -u origin your-feature`)


# Contributing Guidelines

1. Make sure your master branch is up-to-date before creating a branch to develop your feature.
2. 4 spaces / tab indentation.
3. Keep open braces on the same line as a method, like such: `public void foo(String bar) {`, same goes for if, switch, etc.
4. We are using Java 8. Deal with it.
5. Please use standard Java code conventions throughout your code. If you are not aware of what these are then a copy can be obtained [here](http://www.oracle.com/technetwork/java/codeconventions-150003.pdf).
6. Maven guidelines:
    1. All external dependencies will be handled with Maven.
    2. You may add any repository you require for a dependency.
    3. You may add any dependency you require for your feature.
    4. You must add a scope to every dependency.
        1. You use `<scope>provided</scope>` if you don't want your dependency compiled inside the jar.
        2. You use `<scope>compile</scope>` if you want your dependency compiled inside the jar.
    5. We are using maven-assembly-plugin over maven-shade-plugin to compile dependencies inside the jar.
        1. All you need to do to have your dependency compiled or not inside the jar is add a scope. That's all.
        2. If you wish to complain about assembly-plugin and why we should use shade, don't.
    6. Suggested Maven arguments: `clean compile assembly:single install`.
7. If you need an extra resource - eg. config.yml, <feature-name>.yml, add it in the resources folder and include it in the pom.xml build.
8. Make sure your feature actually works before pushing!
