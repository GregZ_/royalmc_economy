package org.royalmc.economy.database.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;

import org.royalmc.economy.Economy;
import org.royalmc.economy.cache.EconomyData;
import org.royalmc.economy.logging.EconomyLogger;

import net.md_5.bungee.Util;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class SQLManager {
	private final Economy plugin;
	private final EconomyLogger economyLogger;
	private final ConnectionPoolManager pool;
	private String tablePrefix;

	public SQLManager(Economy plugin) {
		this.plugin = plugin;
		economyLogger = plugin.getEconomyLogger();
		pool = new ConnectionPoolManager(plugin);
		tablePrefix = plugin.config.getString("SQL.TablePrefix");
		makeTable();
	}

	public void onDisable() {
		pool.closePool();
	}

	private void makeTable() {
		Connection conn = null;
		try {
			conn = pool.getConnection();
			conn.prepareStatement("CREATE TABLE IF NOT EXISTS `" + tablePrefix + "economy` (`uuid` varchar(36) NOT NULL, `lastname` varchar(16) NOT NULL, `coins` BIGINT default '0', `crystals` BIGINT default '0', PRIMARY KEY (uuid));").executeUpdate();
		} catch (SQLException e) {
			economyLogger.log(Level.SEVERE, ChatColor.RED + "An error occurred when creating the table '" + tablePrefix + "economy' in the database!");
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					economyLogger.log(Level.SEVERE, ChatColor.RED + "An error occurred while closing the connection " + conn.toString() + "!");
					e.printStackTrace();
				}
			}
		}
	}

	public void updateUserName(ProxiedPlayer p) {
		Connection conn = null;
		try {
			conn = pool.getConnection();
			PreparedStatement ps = conn.prepareStatement("UPDATE `" + tablePrefix + "economy` SET `lastname`=? WHERE uuid = ?");
			ps.setString(1, p.getName());
			ps.setString(2, p.getUniqueId().toString());
			ps.executeUpdate();
			economyLogger.log(Level.INFO, ChatColor.GREEN + "Successfully updated name of player " + p.getName() + " (uuid: " + p.getUniqueId().toString() + ") in the database!");
		} catch (SQLException e) {
			economyLogger.log(Level.SEVERE, ChatColor.RED + "An error occurred when updating name of player " + p.getName() + " (uuid: " + p.getUniqueId().toString() + ") in the database!");
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					economyLogger.log(Level.SEVERE, ChatColor.RED + "An error occurred while closing the connection " + conn.toString() + "!");
					e.printStackTrace();
				}
			}
		}
	}

	public EconomyData loadUser(String test) {
		Connection conn = null;
		EconomyData ed = null;
		try {
			if (plugin.getEconomyDataCache().getEconomyDataCache().containsKey(test)) {
				ed = plugin.getEconomyDataCache().getEconomyDataCache().get(test);
			} else {
				conn = pool.getConnection();
				PreparedStatement ps = conn.prepareStatement("SELECT * FROM `" + tablePrefix + "economy` WHERE uuid = ? OR lastname = ? LIMIT 1");
				ps.setString(1, test);
				ps.setString(2, test);
				ResultSet rs = ps.executeQuery();
				if (rs.next()) {
					ed = new EconomyData(plugin, rs.getString("uuid"), rs.getString("lastname"), rs.getLong("crystals"), rs.getLong("coins"));
				}
			}
		} catch (SQLException e) {
			economyLogger.log(Level.SEVERE, ChatColor.RED + "An error occurred while loading user (" + test + ")!");
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					economyLogger.log(Level.SEVERE, ChatColor.RED + "An error occurred while closing the connection " + conn.toString() + "!");
					e.printStackTrace();
				}
			}
		}
		return ed;
	}

	public void initUser(ProxiedPlayer p) {
		Connection conn = null;
		ResultSet rs = null;
		try {
			conn = pool.getConnection();
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM `" + tablePrefix + "economy` WHERE uuid = ? LIMIT 1");
			ps.setString(1, p.getUniqueId().toString());
			rs = ps.executeQuery();
			if (rs.next()) {
				if (!(p.getName().equals(rs.getString("lastname")))) {
					updateUserName(p);
				}
				EconomyData ed = new EconomyData(plugin, p.getUniqueId().toString(), p.getName(), rs.getLong("crystals"), rs.getLong("coins"));
				plugin.getEconomyDataCache().addData(p.getUniqueId().toString(), ed);
			} else {
				PreparedStatement ps2 = conn.prepareStatement("INSERT INTO `" + tablePrefix + "economy`(`uuid`, `lastname`) VALUES (?,?)");
				ps2.setString(1, p.getUniqueId().toString());
				ps2.setString(2, p.getName());
				ps2.executeUpdate();
				plugin.getEconomyDataCache().addData(p.getUniqueId().toString(), new EconomyData(plugin, p.getUniqueId().toString(), p.getName(), 0, 0));
				economyLogger.log(Level.INFO, ChatColor.YELLOW + "Player " + p.getName() + " (uuid: " + p.getUniqueId().toString() + ") could not be found in the database so they have been added to it.");
			}
		} catch (SQLException e) {
			economyLogger.log(Level.SEVERE, ChatColor.RED + "An error occurred while initialising user " + p.getName() + " uuid: " + p.getUniqueId().toString() + "!");
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					economyLogger.log(Level.SEVERE, ChatColor.RED + "An error occurred while closing the connection " + conn.toString() + "!");
					e.printStackTrace();
				}
			}
		}
	}

	public void updateUserData(String uuid, long coins, long crystals) {
		Connection conn = null;
		try {
			conn = pool.getConnection();
			PreparedStatement ps = conn.prepareStatement("UPDATE `" + tablePrefix + "economy` SET `crystals` = ?,`coins` = ? WHERE uuid = ?");
			ps.setLong(1, crystals);
			ps.setLong(2, coins);
			ps.setString(3, uuid);
			ps.executeUpdate();
		} catch (SQLException e) {
			economyLogger.log(Level.SEVERE, ChatColor.RED + "An error occurred when updating the coins and crystals of player " + plugin.getProxy().getPlayer(Util.getUUID(uuid)).getName() + " (uuid: " + uuid + ") in the database!");
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					economyLogger.log(Level.SEVERE, ChatColor.RED + "An error occurred while closing the connection " + conn.toString() + "!");
					e.printStackTrace();
				}
			}
		}
	}

	public ArrayList<String> getTopTenCoins() {
		Connection conn = null;
		ArrayList<String> topTen = new ArrayList<>();
		try {
			conn = pool.getConnection();
			ResultSet rs = conn.prepareStatement("SELECT lastname, coins FROM `" + tablePrefix + "economy` ORDER by coins desc LIMIT 10").executeQuery();
			while (rs.next()) {
				topTen.add(rs.getString("lastname") + "//" + rs.getLong("coins"));
			}
		} catch (SQLException e) {
			economyLogger.log(Level.SEVERE, ChatColor.RED + "An error occurred while generating the top ten coins list!");
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					economyLogger.log(Level.SEVERE, ChatColor.RED + "An error occurred while closing the connection " + conn.toString() + "!");
					e.printStackTrace();
				}
			}
		}
		return topTen;
	}

	public ArrayList<String> getTopTenCrystals() {
		Connection conn = null;
		ArrayList<String> topTen = new ArrayList<>();
		try {
			conn = pool.getConnection();
			ResultSet rs = conn.prepareStatement("SELECT lastname, crystals FROM `" + tablePrefix + "economy` ORDER by crystals desc LIMIT 10").executeQuery();
			while (rs.next()) {
				topTen.add(rs.getString("lastname") + " // " + rs.getLong("crystals"));
			}
		} catch (SQLException e) {
			economyLogger.log(Level.SEVERE, ChatColor.RED + "An error occurred while generating the top ten crystals list!");
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					economyLogger.log(Level.SEVERE, ChatColor.RED + "An error occurred while closing the connection " + conn.toString() + "!");
					e.printStackTrace();
				}
			}
		}
		return topTen;
	}
}