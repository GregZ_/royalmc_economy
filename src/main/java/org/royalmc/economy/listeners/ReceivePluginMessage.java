package org.royalmc.economy.listeners;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import org.royalmc.economy.Economy;
import org.royalmc.economy.cache.EconomyData;

import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ReceivePluginMessage implements Listener {

	private final Economy plugin;

	public ReceivePluginMessage(Economy economy) {
		this.plugin = economy;
	}

	@EventHandler
	public void onReceivePluginMessage(PluginMessageEvent event) {
		if (!(event.getTag().equals("RoyalMC_Economy"))) {
			return;
		}

		// TODO add events to fire
		// If other developers want this added please tell me on Skype or in an issue on GitLab
		event.setCancelled(true);
		DataInputStream in = new DataInputStream(new ByteArrayInputStream(event.getData()));

		try {
			String subChannel = in.readUTF();
			String sendingServerName = null;
			String uuid = null;
			long amount = 0;
			boolean success = false;
			EconomyData ed = null;

			switch (subChannel) {
			case "EconomyData":
				sendingServerName = in.readUTF();
				uuid = in.readUTF();
				ed = plugin.getEconomyDataCache().getPlayerData(uuid);

				plugin.getPluginMessageSender().sendEconomyData(sendingServerName, uuid, ed.getPlayerName(), ed.getCoins(), ed.getCrystals());
				break;
			case "SetCoins":
				uuid = in.readUTF();
				ed = plugin.getEconomyDataCache().getPlayerData(uuid);

				amount = in.readLong();
				if (amount > 0 && amount <= 9223372036854775807l) {
					ed.setCoins(amount);
					ed.updateDatabase();
					success = true;
				}

				plugin.getPluginMessageSender().sendSetCoins(uuid, ed.getPlayerName(), ed.getCoins(), amount, success);
				break;
			case "ResetCoins":
				uuid = in.readUTF();
				ed = plugin.getEconomyDataCache().getPlayerData(uuid);

				ed.setCoins(0);
				ed.updateDatabase();

				plugin.getPluginMessageSender().sendResetCoins(uuid, ed.getPlayerName());
				break;
			case "CreditCoins":
				uuid = in.readUTF();
				ed = plugin.getEconomyDataCache().getPlayerData(uuid);

				amount = in.readLong();
				ed.addCoins(amount);
				ed.updateDatabase();

				plugin.getPluginMessageSender().sendCreditCoins(uuid, ed.getPlayerName(), ed.getCoins(), amount);
				break;
			case "DebitCoins":
				uuid = in.readUTF();
				ed = plugin.getEconomyDataCache().getPlayerData(uuid);

				amount = in.readLong();
				success = ed.removeCoins(amount);

				if (success) {
					ed.updateDatabase();
				}

				plugin.getPluginMessageSender().sendDeditCoins(uuid, ed.getPlayerName(), ed.getCoins(), amount, success);
				break;
			case "SetCrystals":
				uuid = in.readUTF();
				ed = plugin.getEconomyDataCache().getPlayerData(uuid);

				amount = in.readLong();
				if (amount > 0 && amount <= 9223372036854775807l) {
					ed.setCrystals(amount);
					ed.updateDatabase();
					success = true;
				}

				plugin.getPluginMessageSender().sendSetCrystals(uuid, ed.getPlayerName(), ed.getCoins(), amount, success);
				break;
			case "ResetCrystals":
				uuid = in.readUTF();
				ed = plugin.getEconomyDataCache().getPlayerData(uuid);

				ed.setCrystals(0);
				ed.updateDatabase();

				plugin.getPluginMessageSender().sendResetCrystals(uuid, ed.getPlayerName());
				break;
			case "CreditCrystals":
				uuid = in.readUTF();
				ed = plugin.getEconomyDataCache().getPlayerData(uuid);

				amount = in.readLong();
				ed.addCrystals(amount);
				ed.updateDatabase();

				plugin.getPluginMessageSender().sendCreditCrystals(uuid, ed.getPlayerName(), ed.getCoins(), amount);
				break;
			case "DebitCrystals":
				uuid = in.readUTF();
				ed = plugin.getEconomyDataCache().getPlayerData(uuid);

				amount = in.readLong();
				success = ed.removeCrystals(amount);

				if (success) {
					ed.updateDatabase();
				}

				plugin.getPluginMessageSender().sendDeditCrystals(uuid, ed.getPlayerName(), ed.getCoins(), amount, success);

				break;
			case "TopTenCoins":
				plugin.getPluginMessageSender().sendTopTenCoins(in.readUTF());
				break;
			case "TopTenCrystals":
				plugin.getPluginMessageSender().sendTopTenCrystals(in.readUTF());
				break;
			default:
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
