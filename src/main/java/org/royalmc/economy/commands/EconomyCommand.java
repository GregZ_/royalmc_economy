package org.royalmc.economy.commands;

import org.royalmc.economy.Economy;
import org.royalmc.economy.commands.arguments.GiveArgument;
import org.royalmc.economy.commands.arguments.ResetArgument;
import org.royalmc.economy.commands.arguments.SetArgument;
import org.royalmc.economy.commands.arguments.TakeArgument;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.plugin.Command;

public class EconomyCommand extends Command {

	private Economy plugin;

	private BaseComponent[] main;
	private BaseComponent[] give;
	private BaseComponent[] take;
	private BaseComponent[] set;
	private BaseComponent[] reset;

	public EconomyCommand(Economy plugin) {
		super("economy", null, "eco");
		this.plugin = plugin;
		setupLists();
	}

	private void setupLists() {
		main = new ComponentBuilder("[").color(ChatColor.DARK_GRAY)
				.append(plugin.getDescription().getName()).color(ChatColor.LIGHT_PURPLE)
				.append("] ").color(ChatColor.DARK_GRAY)
				.append("Version: " + plugin.getDescription().getVersion()).color(ChatColor.GOLD)
				.append(" -- ").color(ChatColor.GRAY)
				.append("Plugin developed by ").color(ChatColor.GOLD)
				.append("GregZ_").color(ChatColor.DARK_GREEN).create();

		give = new ComponentBuilder("/").color(ChatColor.DARK_GRAY)
				.append("economy give <PLAYERNAME> <COINS/CRYSTALS> <AMOUNT>").color(ChatColor.GOLD)
				.append(" - ").color(ChatColor.GRAY)
				.append("Add the specified amount to the players coins / crystals.").color(ChatColor.YELLOW).create();

		take = new ComponentBuilder("/").color(ChatColor.DARK_GRAY)
				.append("economy take <PLAYERNAME> <COINS/CRYSTALS> <AMOUNT>").color(ChatColor.GOLD)
				.append(" - ").color(ChatColor.GRAY)
				.append("Take the specified amount from the players coins / crystals.").color(ChatColor.YELLOW).create();

		set = new ComponentBuilder("/").color(ChatColor.DARK_GRAY)
				.append("economy set <PLAYERNAME> <COINS/CRYSTALS> <AMOUNT>").color(ChatColor.GOLD)
				.append(" - ").color(ChatColor.GRAY)
				.append("Set the specified amount as the players coins / crystals.").color(ChatColor.YELLOW).create();

		reset = new ComponentBuilder("/").color(ChatColor.DARK_GRAY)
				.append("economy reset <PLAYERNAME> <COINS/CRYSTALS>").color(ChatColor.GOLD)
				.append(" - ").color(ChatColor.GRAY)
				.append("Reset the players coins / crystals.").color(ChatColor.YELLOW).create();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (args.length == 0) {

			sender.sendMessage(main);

			if (sender.hasPermission("economy.give")) {
				sender.sendMessage(give);
			}

			if (sender.hasPermission("economy.take")) {
				sender.sendMessage(take);
			}

			if (sender.hasPermission("economy.set")) {
				sender.sendMessage(set);
			}

			if (sender.hasPermission("economy.reset")) {
				sender.sendMessage(reset);
			}
		} else {
			if (args[0].equalsIgnoreCase("give")) {
				new GiveArgument(plugin, sender, args).execute();
				return;
			}

			else if (args[0].equalsIgnoreCase("take")) {
				new TakeArgument(plugin, sender, args).execute();
				return;
			}

			else if (args[0].equalsIgnoreCase("set")) {
				new SetArgument(plugin, sender, args).execute();
				return;
			}

			else if (args[0].equalsIgnoreCase("reset")) {
				new ResetArgument(plugin, sender, args).execute();
				return;
			}
		}
	}
}
