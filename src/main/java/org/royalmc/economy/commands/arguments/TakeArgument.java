package org.royalmc.economy.commands.arguments;

import org.royalmc.economy.Economy;
import org.royalmc.economy.cache.EconomyData;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class TakeArgument {

	private Economy plugin;
	private CommandSender sender;
	private String[] args;

	public TakeArgument(Economy plugin, CommandSender sender, String[] args) {
		this.plugin = plugin;
		this.sender = sender;
		this.args = args;
	}

	public void execute() {
		if (!(sender.hasPermission("economy.take"))) {
			sender.sendMessage(new ComponentBuilder("You do not have permission to do this!").color(ChatColor.RED).create());
			return;
		}

		if (args.length == 1) {
			sender.sendMessage(new ComponentBuilder("/").color(ChatColor.DARK_GRAY)
					.append("economy take <PLAYERNAME> <COINS/CRYSTALS> <AMOUNT>").color(ChatColor.GOLD)
					.append(" - ").color(ChatColor.GRAY)
					.append("Take the specified amount from the players coins / crystals.").color(ChatColor.YELLOW).create());
			return;
		}

		else {
			if (args.length < 4) {
				sender.sendMessage(new ComponentBuilder("Insufficient arguments!").color(ChatColor.RED).create());
				sender.sendMessage(new ComponentBuilder("/").color(ChatColor.DARK_GRAY)
						.append("economy take <PLAYERNAME> <COINS/CRYSTALS> <AMOUNT>").color(ChatColor.GOLD)
						.append(" - ").color(ChatColor.GRAY)
						.append("Take the specified amount from the players coins / crystals.").color(ChatColor.YELLOW).create());
				return;
			}
			EconomyData ed = plugin.getEconomyDataCache().getPlayerData(args[1]);
			if (ed == null) {
				sender.sendMessage(new ComponentBuilder("Could not find player with name or uuid " + args[1] + "!").color(ChatColor.RED).create());
				return;
			}

			long amount;

			if (args[2].equalsIgnoreCase("coins")) {
				try {
					amount = Long.parseLong(args[3]);
					if (ed.removeCoins(amount)) {
						sender.sendMessage(new ComponentBuilder("Sucesfully removed " + amount + " coins from " + args[1] + ".").color(ChatColor.GREEN).create());
						ed.updateDatabase();
						plugin.getPluginMessageSender().sendDeditCoins(ed.getUUID(), ed.getPlayerName(), ed.getCoins(), amount, true);
					} else {
						sender.sendMessage(new ComponentBuilder("Player " + args[1] + " does not have enough coins to remove " + amount + "!").color(ChatColor.RED).create());
					}
				} catch (NumberFormatException e) {
					sender.sendMessage(new ComponentBuilder(args[2] + " is not a number" + "!").color(ChatColor.RED).create());
				}
			} else if (args[2].equalsIgnoreCase("crystals")) {
				try {
					amount = Long.parseLong(args[3]);
					if (ed.removeCrystals(amount)) {
						sender.sendMessage(new ComponentBuilder("Sucesfully removed " + amount + " crystals from " + args[1] + ".").color(ChatColor.GREEN).create());
						ed.updateDatabase();
						plugin.getPluginMessageSender().sendDeditCrystals(ed.getUUID(), ed.getPlayerName(), ed.getCrystals(), amount, true);
					} else {
						sender.sendMessage(new ComponentBuilder("Player " + args[1] + " does not have enough crystals to remove " + amount + "!").color(ChatColor.RED).create());
					}
				} catch (NumberFormatException e) {
					sender.sendMessage(new ComponentBuilder(args[2] + " is not a number" + "!").color(ChatColor.RED).create());
				}
			} else {
				sender.sendMessage(new ComponentBuilder(args[2] + " is not a valid input it must be coins or crystals" + "!").color(ChatColor.RED).create());
			}
		}
	}
}
