package org.royalmc.economy.commands.arguments;

import org.royalmc.economy.Economy;
import org.royalmc.economy.cache.EconomyData;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class GiveArgument {

	private Economy plugin;
	private CommandSender sender;
	private String[] args;

	public GiveArgument(Economy plugin, CommandSender sender, String[] args) {
		this.plugin = plugin;
		this.sender = sender;
		this.args = args;
	}

	public void execute() {
		if (!(sender.hasPermission("economy.give"))) {
			sender.sendMessage(new ComponentBuilder("You do not have permission to do this!").color(ChatColor.RED).create());
			return;
		}

		if (args.length == 1) {
			sender.sendMessage(new ComponentBuilder("/").color(ChatColor.DARK_GRAY)
					.append("economy give <PLAYERNAME> <COINS/CRYSTALS> <AMOUNT>").color(ChatColor.GOLD)
					.append(" - ").color(ChatColor.GRAY)
					.append("Add the specified amount to the players coins / crystals.").color(ChatColor.YELLOW).create());
			return;
		}

		else {
			if (args.length < 4) {
				sender.sendMessage(new ComponentBuilder("Insufficient arguments!").color(ChatColor.RED).create());
				sender.sendMessage(new ComponentBuilder("/").color(ChatColor.DARK_GRAY)
						.append("economy give <PLAYERNAME> <COINS/CRYSTALS> <AMOUNT>").color(ChatColor.GOLD)
						.append(" - ").color(ChatColor.GRAY)
						.append("Add the specified amount to the players coins / crystals.").color(ChatColor.YELLOW).create());
				return;
			}
			EconomyData ed = plugin.getEconomyDataCache().getPlayerData(args[1]);
			if (ed == null) {
				sender.sendMessage(new ComponentBuilder("Could not find player with name or uuid " + args[1] + "!").color(ChatColor.RED).create());
				return;
			}

			long amount;

			if (args[2].equalsIgnoreCase("coins")) {
				try {
					amount = Long.parseLong(args[3]);
					ed.addCoins(amount);
					ed.updateDatabase();
					plugin.getPluginMessageSender().sendCreditCoins(ed.getUUID(), ed.getPlayerName(), ed.getCoins(), amount);
					sender.sendMessage(new ComponentBuilder("Sucesfully gave " + amount + " coins to " + args[1] + ".").color(ChatColor.GREEN).create());
				} catch (NumberFormatException e) {
					sender.sendMessage(new ComponentBuilder(args[2] + " is not a number" + "!").color(ChatColor.RED).create());
				}
			} else if (args[2].equalsIgnoreCase("crystals")) {
				try {
					amount = Long.parseLong(args[3]);
					ed.addCrystals(amount);
					ed.updateDatabase();
					plugin.getPluginMessageSender().sendCreditCrystals(ed.getUUID(), ed.getPlayerName(), ed.getCrystals(), amount);
					sender.sendMessage(new ComponentBuilder("Sucesfully gave " + amount + " crystals to " + args[1] + ".").color(ChatColor.GREEN).create());
				} catch (NumberFormatException e) {
					sender.sendMessage(new ComponentBuilder(args[2] + " is not a number" + "!").color(ChatColor.RED).create());
				}
			} else {
				sender.sendMessage(new ComponentBuilder(args[2] + " is not a valid input it must be coins or crystals" + "!").color(ChatColor.RED).create());
			}
		}
	}
}
