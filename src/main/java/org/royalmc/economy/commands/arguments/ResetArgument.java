package org.royalmc.economy.commands.arguments;

import org.royalmc.economy.Economy;
import org.royalmc.economy.cache.EconomyData;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class ResetArgument {

	private Economy plugin;
	private CommandSender sender;
	private String[] args;

	public ResetArgument(Economy plugin, CommandSender sender, String[] args) {
		this.plugin = plugin;
		this.sender = sender;
		this.args = args;
	}

	public void execute() {
		if (!(sender.hasPermission("economy.reset"))) {
			sender.sendMessage(new ComponentBuilder("You do not have permission to do this!").color(ChatColor.RED).create());
			return;
		}

		if (args.length == 1) {
			sender.sendMessage(new ComponentBuilder("/").color(ChatColor.DARK_GRAY)
					.append("economy reset <PLAYERNAME> <COINS/CRYSTALS>").color(ChatColor.GOLD)
					.append(" - ").color(ChatColor.GRAY)
					.append("Reset the players coins / crystals.").color(ChatColor.YELLOW).create());
			return;
		}

		else {
			if (args.length < 3) {
				sender.sendMessage(new ComponentBuilder("Insufficient arguments!").color(ChatColor.RED).create());
				sender.sendMessage(new ComponentBuilder("/").color(ChatColor.DARK_GRAY)
						.append("economy reset <PLAYERNAME> <COINS/CRYSTALS>").color(ChatColor.GOLD)
						.append(" - ").color(ChatColor.GRAY)
						.append("Reset the players coins / crystals.").color(ChatColor.YELLOW).create());
				return;
			}
			EconomyData ed = plugin.getEconomyDataCache().getPlayerData(args[1]);
			if (ed == null) {
				sender.sendMessage(new ComponentBuilder("Could not find player with name or uuid " + args[1] + "!").color(ChatColor.RED).create());
				return;
			}

			if (args[2].equalsIgnoreCase("coins")) {
				ed.setCoins(0);
				ed.updateDatabase();
				plugin.getPluginMessageSender().sendResetCoins(ed.getUUID(), ed.getPlayerName());
				sender.sendMessage(new ComponentBuilder("Sucesfully reset coins of player " + args[1] + "!").color(ChatColor.GREEN).create());
			} else if (args[2].equalsIgnoreCase("crystals")) {
				ed.setCrystals(0);
				ed.updateDatabase();
				plugin.getPluginMessageSender().sendResetCrystals(ed.getUUID(), ed.getPlayerName());
				sender.sendMessage(new ComponentBuilder("Sucesfully reset crystals of player " + args[1] + "!").color(ChatColor.GREEN).create());
			} else {
				sender.sendMessage(new ComponentBuilder(args[2] + " is not a valid input it must be coins or crystals!").color(ChatColor.RED).create());
			}
		}
	}
}
