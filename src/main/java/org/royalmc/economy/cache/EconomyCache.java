package org.royalmc.economy.cache;

import java.util.ArrayList;
import java.util.HashMap;

import org.royalmc.economy.Economy;
import org.royalmc.economy.database.sql.SQLManager;

import net.md_5.bungee.api.connection.ProxiedPlayer;

public class EconomyCache {

	private Economy plugin;
	private HashMap<String, EconomyData> economyDataCache = new HashMap<>();
	private long refreshTimeMillis = 60000;
	private ArrayList<String> topTenCoins = new ArrayList<>();
	private long coinsCreationTime;
	private ArrayList<String> topTenCrystals = new ArrayList<>();
	private long crystalsCreationTime;

	public EconomyCache(Economy plugin) {
		this.plugin = plugin;
		for (ProxiedPlayer p : plugin.getProxy().getPlayers()) {
			plugin.getSqlManager().initUser(p);
		}
		topTenCoins = plugin.getSqlManager().getTopTenCoins();
		coinsCreationTime = System.currentTimeMillis();
		topTenCrystals = plugin.getSqlManager().getTopTenCrystals();
		crystalsCreationTime = System.currentTimeMillis();
	}

	/**
	 * @return the economyDataCache
	 */
	public HashMap<String, EconomyData> getEconomyDataCache() {
		return economyDataCache;
	}

	/**
	 * Add a players EconomyData to the cache or update it id it is already there.
	 * 
	 * @param uuid
	 *            The UUID of the player to add to the cache.
	 * @param economyData
	 *            The EconomyData relevant to the player related to the UUID.
	 */
	public void addData(String uuid, EconomyData economyData) {
		economyDataCache.put(uuid, economyData);
	}

	/**
	 * Get a players cached data from the cache.
	 * 
	 * @param test
	 *            The UUID of the player for which the data will be returned.
	 * @return The players EconomyData or null if no economy data is loaded for that player.
	 */
	public EconomyData getPlayerData(String test) {
		for (EconomyData ed : economyDataCache.values()) {
			if (ed.getPlayerName() == test || ed.getUUID() == test) {
				if (System.currentTimeMillis() - ed.getCreationTime() >= refreshTimeMillis) {
					economyDataCache.put(ed.getUUID(), plugin.getSqlManager().loadUser(ed.getUUID()));
				}
				return ed;
			}
		}
		return plugin.getSqlManager().loadUser(test);
	}

	/**
	 * This will update the EconomyDataCache for all players currently connected to the BungeeCord proxy.
	 */
	public void pullAllEconomyData() {
		economyDataCache.clear();
		SQLManager sql = plugin.getSqlManager();
		plugin.getProxy().getPlayers().stream().forEach(p -> sql.initUser(p));
	}

	public ArrayList<String> getTopTenCoins() {
		if (System.currentTimeMillis() - coinsCreationTime >= refreshTimeMillis) {
			topTenCoins = plugin.getSqlManager().getTopTenCoins();
		}
		return topTenCoins;
	}

	public ArrayList<String> getTopTenCrystals() {
		if (System.currentTimeMillis() - crystalsCreationTime >= refreshTimeMillis) {
			topTenCrystals = plugin.getSqlManager().getTopTenCrystals();
		}
		return topTenCrystals;
	}
}
