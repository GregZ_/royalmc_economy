package org.royalmc.economy;

import org.royalmc.economy.cache.EconomyCache;
import org.royalmc.economy.commands.EconomyCommand;
import org.royalmc.economy.database.ConfigLoader;
import org.royalmc.economy.database.sql.SQLManager;
import org.royalmc.economy.listeners.PlayerListener;
import org.royalmc.economy.listeners.ReceivePluginMessage;
import org.royalmc.economy.logging.EconomyLogger;
import org.royalmc.economy.sender.PluginMessageSender;

import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;

public class Economy extends Plugin {

	public Configuration config;

	private ConfigLoader configLoader;
	private SQLManager sqlManager;

	private PluginMessageSender pluginMessageSender;

	private EconomyLogger economyLogger;

	private EconomyCache economyCache;

	@Override
	public void onEnable() {
		economyLogger = new EconomyLogger(this);
		configLoader = new ConfigLoader(this);
		sqlManager = new SQLManager(this);

		pluginMessageSender = new PluginMessageSender(this);

		getProxy().registerChannel("RoyalMC_Economy");

		economyCache = new EconomyCache(this);
		economyCache.pullAllEconomyData();

		getProxy().getPluginManager().registerListener(this, new ReceivePluginMessage(this));
		getProxy().getPluginManager().registerListener(this, new PlayerListener(this));
		getProxy().getPluginManager().registerCommand(this, new EconomyCommand(this));
	}

	@Override
	public void onDisable() {
		sqlManager.onDisable();
	}

	// API
	public ConfigLoader getConfigLoader() {
		return configLoader;
	}

	public SQLManager getSqlManager() {
		return sqlManager;
	}

	public PluginMessageSender getPluginMessageSender() {
		return pluginMessageSender;
	}

	public EconomyLogger getEconomyLogger() {
		return economyLogger;
	}

	public EconomyCache getEconomyDataCache() {
		return economyCache;
	}
}
